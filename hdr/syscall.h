/*
** syscall.h for strace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Tue May  8 15:37:18 2012 matthieu ciappara
** Last update Sun May 13 13:46:54 2012 matthieu ciappara
*/

#ifndef	SYSCALL_H_
# define		SYSCALL_H_

# define		BUF_SZ			(512)
# define		TRACE_EXISTING_PROCESS	0
# define		TRACE_CHILD_PROCESS	1


typedef unsigned int	t_uint;
typedef unsigned char	t_uchar;

typedef struct		s_syscall
{
  int			opcode;
  char			syscall[BUF_SZ];
}			t_syscall;

typedef union		u_lchar
{
  long			val;
  t_uchar		tab[sizeof(long)];
}			t_lchar;



# ifdef	_EXPORT_G_SYS_LS
extern t_syscall	g_sys_ls[];
# endif	 /* _EXPORT_G_SYS_LS */

int	father(pid_t spoon, t_uint trace_type);
int	child(char **args);
int	trace_existing_process(pid_t pid);
void	set_sig_handler(pid_t spoon, t_uint trace_type);

#endif /* SYSCALL_H_ */
