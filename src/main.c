/*
** main.c for strace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 10:00:42 2012 matthieu ciappara
** Last update Sun May 13 17:56:18 2012 matthieu ciappara
*/

#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "syscall.h"

static int	usage(char *prog)
{
  fprintf(stderr, "Usage :\n\t%s <-p pid>\n"
	  "\t%s <progname> [arguments] [...]\n", prog, prog);
  return (1);
}

static char	**init_args(int argc, char **argv)
{
  char		**args;
  int		i;

  args = malloc(argc * sizeof(*args));
  if (!args)
    {
      fprintf(stderr, "Error : malloc (%s, %d)\n", __FILE__, __LINE__);
      return (NULL);
    }
  args[0] = argv[1];
  i = 1;
  while (i < argc)
    {
      args[i] = argv[i + 1];
      i++;
    }
  return (args);
}

int		main(int argc, char **argv)
{
  pid_t		spoon;
  char		**args;
  int		existing_process;

  if (argc < 2)
    return (usage(argv[0]));
  else if ((existing_process = strcmp(argv[1], "-p")) == 0 && argc >= 3)
    return (trace_existing_process(atoi(argv[2])));
  else if (existing_process == 0 && argc < 3)
    return (usage(argv[0]));
  args = init_args(argc, argv);
  if (!args)
    return (2);
  spoon = fork();
  if (spoon == -1)
    {
      fprintf(stderr, "Error : fork (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      free(args);
      return (3);
    }
  if (spoon == 0)
    return (child(args));
  return (father(spoon, TRACE_CHILD_PROCESS));
}
