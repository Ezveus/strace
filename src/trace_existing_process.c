/*
** trace_existing_process.c for strace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 11:04:26 2012 matthieu ciappara
** Last update Sun May 13 17:10:14 2012 matthieu ciappara
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "syscall.h"

int	trace_existing_process(pid_t spoon)
{
  if (ptrace(PTRACE_ATTACH, spoon, 0, 0) == -1)
    {
      fprintf(stderr, "Error : ptrace (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      return (1);
    }
  return (father(spoon, TRACE_EXISTING_PROCESS));
}
