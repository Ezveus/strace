/*
** father.c for strace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Thu May 10 09:58:45 2012 matthieu ciappara
** Last update Sun May 13 17:30:47 2012 matthieu ciappara
*/

#define _EXPORT_G_SYS_LS
#include <unistd.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include "syscall.h"

static int			cmp(t_lchar next_instr,
				    const t_uchar *op_scall,
				    const t_uchar *op_int,
				    const t_uchar *op_senter)
{
  if (next_instr.tab[0] == op_scall[0] && next_instr.tab[1] == op_scall[1])
    return (1);
  else if (next_instr.tab[0] == op_int[0] && next_instr.tab[1] == op_int[1])
    return (1);
  else if (next_instr.tab[0] == op_senter[0] &&
	   next_instr.tab[1] == op_senter[1])
    return (1);
  return (0);
}

static int			is_syscall(pid_t spoon)
{
  struct user_regs_struct	regs;
  t_lchar			next_instr;
  const t_uchar			opcode_syscall[] = {0x0f, 0x05};
  const t_uchar			opcode_int80[] = {0xcd, 0x80};
  const t_uchar			opcode_sysenter[] = {0x0f, 0x34};

  next_instr.val = 0;
  if (ptrace(PTRACE_GETREGS, spoon, 0, &regs) == -1)
    return (0);
  next_instr.val = ptrace(PTRACE_PEEKDATA, spoon, regs.rip, 0);
  if (next_instr.val == -1)
    return (0);
  return (cmp(next_instr, opcode_syscall, opcode_int80, opcode_sysenter));
}

static char			*opcode_to_call(unsigned long int opcode)
{
  char				*call;
  int				i;
  unsigned long int		op;

  i = 0;
  op = g_sys_ls[i].opcode;
  while (g_sys_ls[i].opcode != -1 &&
	 op != opcode)
    {
      i++;
      op = g_sys_ls[i].opcode;
    }
  if (!(call = strdup(g_sys_ls[i].syscall)))
    return (NULL);
  return (call);
}

static void			print_ret(long rax)
{
  if (rax >= 0)
    printf(" = %ld\n", rax);
  else
    printf(" = -1 (errno = %ld : %s)\n", -rax, strerror(-rax));
}

int				father(pid_t spoon, t_uint trace_type)
{
  int				status;
  struct user_regs_struct	regs;

  set_sig_handler(spoon, trace_type);
  printf("=== Process ID %d ===\n", spoon);
  while (1)
    {
      ptrace(PTRACE_SINGLESTEP, spoon, 0, 0);
      wait4(spoon, &status, 0, 0);
      if (WIFSIGNALED(status))
	{
	  printf("=== Exited with signal %d ===\n", WTERMSIG(status));
	  return (WTERMSIG(status));
	}
      if (is_syscall(spoon))
	{
	  ptrace(PTRACE_GETREGS, spoon, 0, &regs);
	  printf("%lu : %s(args)", regs.rax, opcode_to_call(regs.rax));
	  ptrace(PTRACE_SINGLESTEP, spoon, 0, 0);
	  wait4(spoon, &status, 0, 0);
	  ptrace(PTRACE_GETREGS, spoon, 0, &regs);
	  print_ret(regs.rax);
	}
      if (WIFEXITED(status))
	{
	  printf("=== Exited with status %d ===\n", WEXITSTATUS(status));
	  return (WEXITSTATUS(status));
	}
      else if (WIFSIGNALED(status))
	{
	  printf("=== Exited with signal %d ===\n", WTERMSIG(status));
	  return (WTERMSIG(status));
	}
    }
  return (0);
}
