/*
** sig_handler.c for strace in /home/matthieu
** 
** Made by matthieu ciappara
** Login   <ciappa_m@epitech.net>
** 
** Started on  Sun May 13 13:46:05 2012 matthieu ciappara
** Last update Sun May 13 17:43:11 2012 matthieu ciappara
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "syscall.h"

static pid_t	g_spoon = 0;

void	sigint_ex_handler(int sigint)
{
  (void)sigint;
  if (ptrace(PTRACE_DETACH, g_spoon, 0, 0) == -1)
    {
      fprintf(stderr, "\rError : ptrace (%s, %d) : %s\n",
	      __FILE__, __LINE__, strerror(errno));
      exit(1);
    }
  printf("Sigint_ex_handler called : exiting\n");
  exit(0);
}

void	set_sig_handler(pid_t spoon, t_uint trace_type)
{
  g_spoon = spoon;
  if (trace_type == TRACE_EXISTING_PROCESS)
    {
      signal(SIGINT, sigint_ex_handler);
      printf("Sigint_ex_handler set\n");
    }
  else if (trace_type == TRACE_CHILD_PROCESS)
    {
    }
}
